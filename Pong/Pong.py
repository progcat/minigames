import pygame
import os
#local Class
import tile
import ball

window_size = 1080, 720
bg_color = 0, 0, 0
obj_color = 255, 255, 255
obj_size = 5
caption = "Pong By GC"
game_end = False
tiles_speed = 2

def main():
    # init game
    pygame.init()
    screen = pygame.display.set_mode(window_size)
    pygame.display.set_caption(caption)

    #Tiles
    tile1_pos1 = ((12), (window_size[1] / 2) - 90)
    tile1_pos2 = ((12), (window_size[1] / 2) + 90)
    tile1 = tile.Tile(screen, (tile1_pos1, tile1_pos2), obj_color, tiles_speed,10)

    tile2_pos1 = ((window_size[0] - 12), (window_size[1] / 2) - 90)
    tile2_pos2 = ((window_size[0] - 12), (window_size[1] / 2) + 90)
    tile2 = tile.Tile(screen, (tile2_pos1, tile2_pos2), obj_color, tiles_speed,10)

    ball_obj = ball.Ball(screen, (int(window_size[0]/2), int(window_size[1]/2)), obj_color, 2, 2, 10)

    while 1:
        #Event handler
        for event in pygame.event.get():
            #Close game event
            if event.type == 12:
                print("Exit")
                exit()
        key = pygame.key.get_pressed()
        if key[pygame.K_UP]:
            tile2.goUp()
        if key[pygame.K_DOWN]:
            tile2.goDown()
        if key[pygame.K_w]:
            tile1.goUp()
        if key[pygame.K_s]:
            tile1.goDown()
        #draw background
        screen.fill(bg_color)
        #draw middle line
        pygame.draw.line(screen, obj_color, (window_size[0] / 2, 0), (window_size[0] / 2, window_size[1]), obj_size)
        if ball_obj.vector[0] < tile1.from_pos[0] or ball_obj.vector[0] > tile2.from_pos[0]:
            ball_obj.setVector((window_size[0]/2, window_size[1]/2))
        ball_obj.update(tile1, tile2)
        tile1.update()
        tile2.update()
        pygame.display.update()
        #Loop end
#start here
if __name__ == "__main__":
    main()

