import os, pygame, Pong

class Ball:
	screen = None
	radius = 0
	vector = (0,0)
	color = (0,0,0)
	xspeed = 0
	yspeed = 0
	def __init__(self, scr, vect, col, xs, ys, Size=5):
		self.screen = scr
		self.vector = vect
		self.color = col
		self.xspeed = xs
		self.yspeed = ys
		self.radius = Size
	def setVector(self, vect):
		self.vector = vect

	def update(self, tile1, tile2):
		if self.vector[0] < tile1.from_pos[0] + 10 and (self.vector[1] > tile1.from_pos[1] and self.vector[1] < tile1.to_pos[1]):
			self.xspeed *= -1
		
		elif self.vector[0] > tile2.from_pos[0] - 10 and (self.vector[1] > tile2.from_pos[1] and self.vector[1] < tile2.to_pos[1]):
			self.xspeed *= -1
				
		if self.vector[1] > Pong.window_size[1] or self.vector[1] < 0:
			self.yspeed *= -1
		
		x = self.vector[0] + self.xspeed
		y = self.vector[1] + self.yspeed
		self.vector = (int(x), int(y))
		pygame.draw.circle(self.screen, self.color, self.vector, self.radius)
	
	def setSpeed(self, xs, ys):
		self.xspeed = xs
		self.yspeed = ys

		