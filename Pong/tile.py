import pygame, os, Pong

class Tile:
    screen = None
    size = 0
    length = 0
    from_pos = 0,0
    to_pos = 0,0
    col = (0,0,0)
    speed = 0;

    def __init__(self, scr, vect, color = (255,255,255), speed = 10, size = 0):
        self.screen = scr
        self.col = color
        self.size = size
        self.to_pos = vect[1]
        self.from_pos = vect[0]
        self.speed = speed

    def setVector(self, vector):
        self.to_pos = vector[1]
        self.from_pos = vector[0]

    def update(self):
        pygame.draw.line(self.screen, self.col, self.from_pos, self.to_pos, self.size)

    def goUp(self):
        
        x1 = self.from_pos[0]
        y1 = self.from_pos[1]
        
        x2 = self.to_pos[0]
        y2 = self.to_pos[1]

        if y1 > 0:
            y1 -= self.speed
            y2 -= self.speed
            self.from_pos = (x1, y1)
            self.to_pos = (x2, y2)
    def goDown(self):
        
        x1 = self.from_pos[0]
        y1 = self.from_pos[1]
        
        x2 = self.to_pos[0]
        y2 = self.to_pos[1]

        if y2 < Pong.window_size[1]: # y2 > max y
            y1 += self.speed
            y2 += self.speed
            self.from_pos = (x1, y1)
            self.to_pos = (x2, y2)