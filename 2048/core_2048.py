import numpy, random

class Board:

    def __init__(self, size=(3,3)):
        self.size = size
        self.board = numpy.zeros(size, int)
        self.score = 0
        self.moved = False

    def getBoard(self):
        return self.board

    def getSize(self):
        return self.size

    def getScore(self):
        return self.score

    def isEmpty(self, x, y):
        if (x < 0 or x > self.size[0])\
        or (y < 0 or y > self.size[1]):
            print("Error from isEmpty(), called with ", x, ", ", y)
            exit()
        if self.board[y][x] == 0:
            return True
        else:
            return False

    def isOver(self):
        for y in range(self.size[0]):
            for x in range(self.size[1]):
                if y > 0 and self.board[y-1][x] == self.board[y][x]:
                    #check y
                    return False
                if self.board[y][x] == 0:
                    return False
                if x != 2 and self.board[y][x] == self.board[y][x+1]:
                    return False
        return True

    def resetBoard(self):
        self.board = numpy.zeros(self.size, int)
        self.score = 0
        #randomize
        tile = 2
        x = random.randint(0, self.size[0] - 1)
        y = random.randint(0, self.size[1] - 1)
        self.board[x][y] = tile

        self.addRandomTiles()

    def addRandomTiles(self):
        x = 0
        y = 0
        tile = random.randint(-1, 1)
        if tile > 0:
            tile = 2
        else:
            tile = 4
        while not self.isEmpty(x, y):
            x = random.randint(0, self.size[0] - 1)
            y = random.randint(0, self.size[1] - 1)
        self.board[x][y] = tile

    def compressing_up(self):
        clone_board = self.board
        #starting from the top
        for y in range(1, self.size[0]):
            for x in range(self.size[1]):
                if clone_board[y][x] != 0:
                    if clone_board[y-1][x] == 0:
                        #got place to go
                        clone_board[y-1][x] = clone_board[y][x]
                        clone_board[y][x] = 0
                        self.moved = True
        return clone_board

    def merge_up(self):
        clone_board = self.board
        for y in range(1, self.size[1]):
            for x in range(self.size[0]):
                if clone_board[y][x] != 0:
                    if clone_board[y][x] == clone_board[y-1][x]:
                        clone_board[y-1][x] = clone_board[y-1][x] * 2
                        clone_board[y][x] = 0
                        self.moved = True
                        self.score += clone_board[y-1][x]
        return clone_board

    def action_up(self):
        self.moved = False
        self.board = self.compressing_up()
        self.board = self.merge_up()
        self.board = self.compressing_up()
        return self.moved

    def action_down(self):
        #mirror
        self.board = numpy.flipud(self.board)
        vm = self.action_up()
        #mirror again
        self.board = numpy.flipud(self.board)
        return vm

    def action_left(self):
        #turn right
        self.board = numpy.rot90(self.board, 1, (1,0))
        #now it face upward
        vm = self.action_up()
        #turn it back
        self.board = numpy.rot90(self.board)
        return vm

    def action_right(self):
        # turn left
        self.board = numpy.rot90(self.board)
        # now it face upward
        vm = self.action_up()
        # turn it back
        self.board = numpy.rot90(self.board, 1, (1, 0))
        return vm


if __name__ == "__main__":
    board = Board()
    board.resetBoard()
    while True:

        if board.isOver():
            print("##########")
            print("Final Score: ", board.getScore())
            board.resetBoard()
        #Action
        valid_move = False
        print(board.getBoard())
        user = input("cmd: ")
        if user == "w":
            valid_move = board.action_up()
        elif user == "s":
            valid_move = board.action_down()
        elif user == "a":
            valid_move = board.action_left()
        elif user == "d":
            valid_move = board.action_right()

        #Add tiles
        if(valid_move):
            board.addRandomTiles()