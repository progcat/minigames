import numpy as np
import random
import os

#mine map
MINE_BOMB = 1
MINE_EMPTY = 0

VIEW_HIDE = 0
VIEW_EMPTY = -1
VIEW_FLAG = 10

ACTION_STEP = 1
ACTION_TAG = 2

def print_map(board):
    for row in board:
        print('|', end='')
        for elem in row:
            print(elem, end='|') 
        print()

class MineSweeper:

    def __init__(self, bomb_n, shape=(10, 10)):
        """
            bomb_n: number of bomb
            shape(height, width): size of the mine field 
        """
        if bomb_n <= 0:
            raise ValueError("bomb_n should be bigger than 0")
        if bomb_n > shape[0] * shape[1]:
            raise ValueError("Too much bomb!")
        self.bomb_n = bomb_n
        self.bomb_total = bomb_n
        if shape[0] <= 0 or shape[1] <= 0:
            raise ValueError("Invalid shape")
        self.shape = shape
        self.mine_field = np.zeros(shape, dtype=np.int8)
        self.view_map = np.zeros(shape, dtype=np.int8)
        self.is_end = False

    def new_game(self):
        self.is_end = False
        self.score = 0
        self.view_map = np.zeros(self.shape, dtype=np.int8)
        self.bomb_n = self.bomb_total

        placed = 0
        #randomly placing bombs
        while placed != self.bomb_total:
            x = random.randrange(0, self.shape[1])
            y = random.randrange(0, self.shape[0])
            if not self.is_bomb(x, y):
                #place bomb
                self.mine_field[y][x] = MINE_BOMB
                placed += 1

    def is_bomb(self, x, y):
        if x < 0 or y < 0 or x >= self.shape[1] or y >= self.shape[0]:
            raise ValueError("Invalid position!")
        if self.mine_field[y][x] == MINE_BOMB:
            return True
        return False

    def count_bombs(self, mid_x, mid_y):
        num = 0
        if mid_x < 0 or mid_y < 0 or mid_x >= self.shape[1] or mid_y >= self.shape[0]:
            raise ValueError("Invalid position!")
        for off_y in range(-1, 2):
            for off_x in range(-1, 2):
                y = mid_y + off_y
                x = mid_x + off_x
                if y < 0 or y >= self.shape[0]:
                    continue 
                if x < 0 or x >= self.shape[1]:
                    continue
                if self.is_bomb(x, y):
                    num += 1
        return num
    
    def explore_empty(self, mid_x, mid_y):
        num = self.count_bombs(mid_x, mid_y)
        #if bombs nearby
        if num > 0:
            self.view_map[mid_y][mid_x] = num
        else:
            self.view_map[mid_y][mid_x] = VIEW_EMPTY
            for off_y in range(-1, 2):
                for off_x in range(-1, 2):
                    y = mid_y + off_y
                    x = mid_x + off_x
                    if y < 0 or y >= self.shape[0]:
                        continue 
                    if x < 0 or x >= self.shape[1]:
                        continue
                    if self.view_map[y][x] == VIEW_HIDE:
                        self.explore_empty(x, y)

    def get_map(self):
        return self.mine_field

    def get_text_map(self):
        text_map = []
        for y in range(self.shape[0]):
            row = []
            for x in range(self.shape[1]):
                cursor = self.view_map[y][x]
                if cursor == VIEW_EMPTY:
                    row.append(' ')
                elif cursor == VIEW_FLAG:
                    row.append('F')
                elif cursor == VIEW_HIDE:
                    row.append('*')
                else:
                    row.append(str(cursor))
            text_map.append(row)
        return np.array(text_map)

    def gameover(self):
        self.is_end = True
        #scoring
        score = 0
        for y in range(self.shape[0]):
            for x in range(self.shape[1]):
                #if tagged as bomb
                if self.view_map[y][x] == VIEW_FLAG:
                    #and it is actually a bomb
                    if self.is_bomb(x, y):
                        score += 1
        return score

    def step_able(self, x, y):
        if x < 0 or y < 0 or x >= self.shape[1] or y >= self.shape[0]:
            print('Invalid position!', '(',self.shape[1], ', ', self.shape[0], ')')
            print('Get: ', x, ', ', y)
            return False
        if self.view_map[y][x] == VIEW_HIDE:
            return True
        print("You cant click here! ", x, ', ', y)
        return False

    def is_ended(self):
        return self.is_end

    def get_score(self):
        return self.score

    def mark_bomb(self, x, y):
        if x < 0 or y < 0 or x >= self.shape[1] or y >= self.shape[0]:
            raise ValueError("Invalid position!")
        if self.view_map[y][x] == VIEW_FLAG:
            self.bomb_n += 1
            self.view_map[y][x] = VIEW_HIDE
        else:
            #place flag on view map
            self.bomb_n -= 1
            self.view_map[y][x] = VIEW_FLAG

    def check_win(self):
        if self.bomb_n != 0:
            return False
        for y in range(self.shape[0]):
            for x in range(self.shape[1]):
                # if tagged as a bomb, but no bomb there
                if self.view_map[y][x] == VIEW_FLAG and not self.is_bomb(x, y):
                    return False
        return True

    def get_remain_bomb(self):
        return self.bomb_n
    
    #
    # return (status, score, view_map)
    # status: 0: nothing, 1: won, -1: lose
    #
    def step(self, action, x, y):
        if action == ACTION_STEP and self.step_able(x, y):
            # if stepped on a bomb
            if self.is_bomb(x, y):
                score = self.gameover()
                return (-1, score, None)
            # else
            self.explore_empty(x, y)

        if action == ACTION_TAG:
            if self.bomb_n == 0:
                print('You can\'t place more flags')
            self.mark_bomb(x, y)
        # if won
        if self.check_win() == True:
            self.is_end = True
            return (1, self.bomb_total, None)
        
        return (0, None, self.view_map)

# The game
if __name__ == '__main__':
    game = MineSweeper(1, (3,3))
    game.new_game()   

    while True:
        game.print_map(game.get_text_map())
        try:
            print('1: Step, 2: Tag')
            cin = input('~> ')
            x = int(input('x: '))
            y = int(input('y: '))
        except EOFError:
            continue
        except ValueError:
            continue
        
        if cin == '1':
            (status, score, tmap) = game.step(ACTION_STEP, x, y)
        elif cin == '2':
            (status, score, tmap) = game.step(ACTION_TAG, x, y)
        else:
            print('error! retry.')
            continue

        if status == 1:
            print('You win!')
        elif status == -1:
            print('You lose!')
            
            
        if status != 0:
            input('press enter to start new game')
            game.new_game()