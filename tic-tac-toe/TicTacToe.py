import pygame

class TicTacToe:
    PLAYER_A = 1
    PLAYER_B = -1
    DRAW = 2

    pad = [0] * 9
    turn = True
    human_play = False
    window_size = (480, 480)
    current_player = PLAYER_A

    def __init__(self, human):
        self.human_play = human
        #Initalize
        pygame.init()
        self.screen = pygame.display.set_mode(self.window_size)
        pygame.display.set_caption("Tic Tac Toe")

    def get_winner(self):
        return self.check_board()

    def reset_game(self):
        #empty the grid
        self.pad = [0] * 9


    def check_side_win(self, side): #1/-1
        #Horizontal
        if (self.pad[0] == side and self.pad[1] == side and self.pad[2] == side) or\
                (self.pad[3] == side and self.pad[4] == side and self.pad[5] == side) or\
                (self.pad[6] == side and self.pad[7] == side and self.pad[8] == side):
            return True
        #vertical
        if (self.pad[0] == side and self.pad[3] == side and self.pad[6] == side) or\
                (self.pad[1] == side and self.pad[4] == side and self.pad[7] == side) or\
                (self.pad[2] == side and self.pad[5] == side and self.pad[8] == side):
            return True
        #slope
        if (self.pad[0] == side and self.pad[4] == side and self.pad[8] == side) or\
                (self.pad[2] == side and self.pad[4] == side and self.pad[6] == side):
            return True
        return False

    def check_board(self):
        scoreA = self.check_side_win(self.PLAYER_A)
        scoreB = self.check_side_win(self.PLAYER_B)

        if not scoreA and not scoreB:
            #No result or Draw
            #NOT DONE YET
            if 0 in self.pad:
                return 0
            else:
                return self.DRAW
        elif scoreA:
            return self.PLAYER_A
        elif scoreB:
            return self.PLAYER_B
        #Should not win/loss at same time

    def select_grid(self, grid):
        if self.turn:
            self.current_player = self.PLAYER_B
        else:
            self.current_player = self.PLAYER_A
        if self.pad[grid] == 0:
            self.pad[grid] = self.current_player
            self.turn = not self.turn
            return True
        return False

    def draw_symbol(self, side, num):
        #Circle for player A
        if side == self.PLAYER_A:
            ##first
            if num == 0:
                pos = (80, 80)
            if num == 1:
                pos = (240, 80)
            if num == 2:
                pos = (400, 80)
            ##second
            if num == 3:
                pos = (80, 240)
            if num == 4:
                pos = (240, 240)
            if num == 5:
                pos = (400, 240)
            ##thrid
            if num == 6:
                pos = (80, 400)
            if num == 7:
                pos = (240, 400)
            if num == 8:
                pos = (400, 400)

            pygame.draw.circle(self.screen, (255,255,255), pos, 75, 2)
        #Cross for player B
        elif side == self.PLAYER_B:
            ##first
            if num == 0:
                posa = (5, 5)
                posb = (155, 155)
            if num == 1:
                posa = (165, 5)
                posb = (315, 155)
            if num == 2:
                posa = (325, 5)
                posb = (475, 155)
            ##second
            if num == 3:
                posa = (5, 165)
                posb = (155, 315)
            if num == 4:
                posa = (165, 165)
                posb = (315, 315)
            if num == 5:
                posa = (325, 165)
                posb = (475, 315)
            ##thrid
            if num == 6:
                posa = (5, 325)
                posb = (155, 475)
            if num == 7:
                posa = (165, 325)
                posb = (315, 475)
            if num == 8:
                posa = (325, 325)
                posb = (475, 475)
            #calculate oppsite line
            posc = (posb[0], posa[1])
            posd = (posa[0], posb[1])
            pygame.draw.line(self.screen, (255,255,255), posa, posb, 3)
            pygame.draw.line(self.screen, (255,255,255), posc, posd, 3)

    def parse_pos(self, pos):
        if pos[1]<160:
            if pos[0]<160:
                return 0
            elif pos[0]<320:
                return 1
            elif pos[0]<480:
                return 2
        elif pos[1]<320:
            if pos[0] < 160:
                return 3
            elif pos[0] < 320:
                return 4
            elif pos[0] < 480:
                return 5
        elif pos[1]<480:
            if pos[0] < 160:
                return 6
            elif pos[0] < 320:
                return 7
            elif pos[0] < 480:
                return 8

    def board_update(self):
        for i in range(9):
            self.draw_symbol(self.pad[i], i)

    def loop(self):
        #clear screen
        self.screen.fill((0,0,0))
        # Event handler
        for event in pygame.event.get():
            # Close game event
            if event.type == pygame.QUIT:
                print("Exit")
                pygame.quit()
                exit()
            #Human player only
            if event.type == pygame.MOUSEBUTTONDOWN and self.human_play == True:
                if pygame.mouse.get_focused():
                    pp = self.parse_pos(pygame.mouse.get_pos())
                    self.select_grid(pp)
        #
        #Draw our board
        #
        #Vertical lines
        pygame.draw.line(self.screen, (255,255,255), (self.window_size[0]/3, 0), (self.window_size[0]/3, self.window_size[1]), 1)
        pygame.draw.line(self.screen, (255, 255, 255), (self.window_size[0]*2/3, 0), (self.window_size[0]*2/3, self.window_size[1]), 1)
        #Horizontal lines
        pygame.draw.line(self.screen, (255, 255, 255), (0, self.window_size[1]/3), (self.window_size[0], self.window_size[1]/3), 1)
        pygame.draw.line(self.screen, (255, 255, 255), (0, self.window_size[1]*2/ 3), (self.window_size[0], self.window_size[1]*2/ 3), 1)
        self.board_update()
        if self.human_play == True:
            result = self.check_board()
            if result == self.PLAYER_A:
                print("Player A win!")
            elif result == self.PLAYER_B:
                print("Player B win!")
            elif result == self.DRAW:
                print("Draw!")
            if result != 0:
                self.reset_game()
        pygame.display.update()

if __name__ == "__main__":
    _3T = TicTacToe(True)
    while True:
        _3T.loop()