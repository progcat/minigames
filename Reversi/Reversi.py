import numpy

NEUTRAL = 0
BLACK = 1
WHITE = -1

directions = [[0, 1], [0, -1], [-1, 0], [1, 0]
            ,[-1, 1], [1, 1], [-1, -1], [1, -1]]

class Reversi:
    def __init__(self, first_player=BLACK):
        self.current_player = first_player
        self.board = numpy.zeros((8, 8), int)

        self.board[3][3] = WHITE
        self.board[3][4] = BLACK
        self.board[4][4] = WHITE
        self.board[4][3] = BLACK

    def PlayTurn(self, x, y):
        if [x, y] in self.GetPossibleMoves():
            self.board[y][x] = self.current_player
            self.UpdateBoard(x, y)
            self.current_player = self.GetOpponent()
        else:
            print('[PlayTurn] (', x, ',', y, ') not available!')
    
    def IsGameOver(self):
        if not(NEUTRAL in self.board):
            #if no space left
            return True
        else:
            # still got space
            moves = self.GetPossibleMoves()
            if len(moves) == 0:
                #if no more legal moves
                return True
        return False

    def CalculateScore(self, player):
        return numpy.count_nonzero(self.board == BLACK) if player == BLACK else numpy.count_nonzero(self.board == WHITE)
    
    def UpdateBoard(self, xin, yin):
        # up, down, left, right, top-left, top-right, down-left, down-right
        for dir in directions:
            posx = xin + dir[0]
            posy = yin + dir[1]
            while True:
                if (posx > -1 and posx < 8) and \
                    (posy > -1 and posy < 8):
                    if self.board[posy][posx] == NEUTRAL:
                        break
                    if self.board[posy][posx] == self.current_player:
                        while [posx, posy] != [xin, yin]:
                            posx -= dir[0]
                            posy -= dir[1]
                            self.board[posy][posx] = self.current_player
                        break
                    posx += dir[0]
                    posy += dir[1]
                else:
                    break
                

    def GetOpponent(self):
        return BLACK if self.current_player == WHITE else WHITE
    
    def GetPossibleMoves(self):
        #they always next to the opposite side and they must be blank
        possible_move = []
        for y in range(8):
            for x in range(8):
                if self.board[y][x] == self.GetOpponent():
                    for dir in directions:
                        posx = x + dir[0]
                        posy = y + dir[1]
                        while True:
                            if (posx > -1 and posx < 8) and \
                                (posy > -1 and posy < 8):
                                if self.board[posy][posx] == NEUTRAL:
                                    break
                                if self.board[posy][posx] == self.current_player:
                                    while True:
                                        if (posx > -1 and posx < 8) and \
                                            (posy > -1 and posy < 8):
                                            if self.board[posy][posx] == NEUTRAL:
                                                if not ([posx, posy] in possible_move):
                                                    possible_move.append([posx, posy])
                                                break
                                            posx -= dir[0]
                                            posy -= dir[1]
                                        else:
                                            break
                                    break

                                posx += dir[0]
                                posy += dir[1]  
                            else:
                                break
                                    
        return possible_move
    

game = Reversi()
while True:
    print('black: ', game.CalculateScore(BLACK))
    print('white: ', game.CalculateScore(WHITE))
    print(game.board)
    print('=========================')
    print('BLACK turn' if game.current_player == BLACK else 'WHITE turn')
    print(game.GetPossibleMoves())
    xin = int(input('X:'))
    yin = int(input('Y:'))
    game.PlayTurn(xin, yin)